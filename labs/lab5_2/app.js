const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const app = express();
const mustache = require('mustache');
const cors = require('cors');
app.use(cors());
const morgan = require('morgan');
app.use(morgan('dev'));

const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');


const busboy = require('busboy-body-parser')

const consolidate = require('consolidate');

//
// … @todo setup app here
//


//
// view engine setup
app.engine('mst', consolidate.swig);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'You can use GET in users and you can use GET,POST,DELETE,PUT in cats ',
            title: 'Lab2: cats and users',
            version: '1.0.0',
        },
        host: 'localhost:3001',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);


// view engine setup
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'data')));

const options2 = {
  limit: '5mb',
  multi: false,
};
app.use(busboy(options2));


app.use('/', indexRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
