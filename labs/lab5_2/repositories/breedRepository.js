const Breed = require('../models/breed');
const JsonStorage = require('../jsonStorage');
const mongoose = require('mongoose');
const fs = require('fs').promises;
const BreedModel = require('../shemas/breed');
class BreedRepository {
 
    constructor(dbUrl) {        
        this.storage = new JsonStorage(dbUrl,BreedModel);
    }
 
    async getBreeds() { 
        await this.storage.connect();
        const result= await this.storage.getItems();
        await this.storage.disconnect();
        return result;
    }
 
    async getBreedById(id) {
        await this.storage.connect();
        const result= await this.storage.getItemById(id);
        await this.storage.disconnect();
        return result;
    }
    async addBreed(breed){
        await this.storage.connect();
        const id= await this.storage.addItem(breed);
        console.log(id);
        await this.storage.disconnect();
        return id;
    }
    async updateBreed(breed){
        await this.storage.connect();
        await this.storage.updateItem(breed);
        await this.storage.disconnect();
    }
    async deleteBreed(id){
        await this.storage.connect();
        await this.storage.deleteItem(id);
        await this.storage.disconnect();
    }
};
 
module.exports = BreedRepository;