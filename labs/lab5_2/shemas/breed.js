const mongoose = require('mongoose');
const BreedSchema = new mongoose.Schema({
    name:{type:String,required:true},
    size:{type:String,required:true},
    age_of_life:{type:Number,required:true},
    avaUrl:{type:String,required:true}
 }, { collection: 'breeds' });
const BreedModel = mongoose.model('Breed',BreedSchema);   
module.exports = BreedModel;