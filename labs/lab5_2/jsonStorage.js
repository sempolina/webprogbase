
const mongoose = require('mongoose');

const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
};


class JsonStorage {

    constructor(dbUrl, model) {
        this.dbUrl = dbUrl;
        this.model=model;
    }
    async connect(){
        console.log(`Server ready`);
        this.client = await mongoose.connect(this.dbUrl, connectOptions);
        console.log('Mongo database connected');

    }
 
    async getItems() {
        const userDocs = await this.model.find().populate("id_breed").populate("id_user");
        return userDocs;
    }
    async addItem(item) {
        console.log("I am dying")
        const result = await this.model(item).save();
        console.log(result);
        return result.id;
    }
    async updateItem(item){
        await this.model.findByIdAndUpdate(item.id, item)
    }
    async deleteItem(id){
        await this.model.findByIdAndRemove(id)
    }
    async getItemById(id){
         const userDocs = await this.model.findById(id).populate("id_breed").populate("id_user");
         return userDocs;
    }
    async disconnect(){
        await this.client.disconnect();
        console.log('Mongo database disconnected');
    }
};
 
module.exports = JsonStorage;
