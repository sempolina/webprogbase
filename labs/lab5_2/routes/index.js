var express = require('express');
var router = express.Router();
const fs = require("fs").promises;
const mustache = require("mustache");

/* GET home page. */
router.get('/', async function(req, res, next) {
  const resultsArray = await Promise.all([
    fs.readFile("./views/partials/head.mst"),
    fs.readFile("./views/partials/header.mst"),
    fs.readFile("./views/partials/footer.mst"),
    fs.readFile("./views/index.mst")
])
const head = resultsArray[0].toString();
const header = resultsArray[1].toString();
const footer = resultsArray[2].toString();
const userTemplate = resultsArray[3].toString();

  const Data = {
    links: [{
      l: "",
      name: "Home"
  },
  {
      l : "/api/cats",
      name: "Cats"
  },
  {
    l: "/api/breeds",
    name: "Breeds"
  },
  {
    l : "/api/users",
    name: "Users"
  },
  {
    l : "/api/about",
    name: "About"
  }
]
 }; 
 const partials = {
  head: head,
  header: header,
  footer: footer
};
res.send(mustache.render(userTemplate, Data, partials))

});

module.exports = router;
