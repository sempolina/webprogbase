const express = require('express');
const router = express.Router();
const contoller=require('../controllers/breeds');
/**
 * We get all breeds
 * @route GET /api/breeds
 * @group Cats - cat operations
 * @param {integer} page.query - number of page
 * @param {integer} per_page.query - number of breeds on page
 * @returns {Array.<Breed>} 200 - get page of Breeds
 */
router.get("/", contoller.getBreeds,contoller.ErrorHandler);
/**
 * We form new breed
 * @route GET /api/breeds/new
 * @group Breeds - breed operations
 * @returns {url} 304 - Breed object
 * @returns {Error} 400 - Bad request
 */
router.get("/new", contoller.new,contoller.ErrorHandler);
/**
 * We add breed
 * @route POST /api/breeds
 * @group Breeds - breed operations
 * @param {file} breed.formData.required - file with breed
 * @returns {Cat.model} 201 - Breed object
 * @returns {Error} 400 - Bad request
 */
router.post('/', contoller.addBreed,contoller.ErrorHandler);
/**
 * We get breed by his id
 * @route GET /api/breeds/{id}
 * @group Breeds - breed operations
 * @param {integer} id.path.required - id of the Breed - eg: 1
 * @returns {Breed.model} 200 - Breed object
 * @returns {Error} 404 - Breed not found
 */
router.get("/:id", contoller.getBreedById,contoller.ErrorHandler);

/**
 * We update breed by his id
 * @route PUT /api/breeds/{id}
 * @group Breed - breed operations
 * @param {integer} id.path.required - id of the Cat - eg: 1
 * @param {file} cat.formData.required- file with cat
 * @returns {Cat.model} 200 - Cat object
 * @returns {Error} 404 - Cat not found
 */
router.put("/:id",contoller.getBreedHandler,contoller.updateBreed,contoller.ErrorHandler);
/**
 * We delete breed by his id
 * @route DELETE /api/breeds/{id}
 * @group Breeds - breed operations
 * @param {integer} id.path.required - id of the Breed - eg: 1
 * @returns {Breed.model} 200 - Breed object
 * @returns {Error} 404 - Breed not found
 */
router.post("/:id", contoller.getBreedHandler,contoller.deleteBreedbyId,contoller.ErrorHandler);
module.exports = router;