current_page = 0;
name = ""
pages_length = 0
async function render(per_page) {
    let element = document.getElementById("search-breed");
    element.setAttribute("disabled",true);
    document.getElementById('pag').innerHTML=""
    document.getElementById('breeds-table').innerHTML=""
    let inn=document.getElementById('cent').innerHTML
    let loading = await fetch("/javascripts/templates/loading.mst")
    loading = await loading.text()
    document.getElementById('cent').innerHTML=loading
    let resp = await fetch("/javascripts/templates/pagination.mst")
    resp = await resp.text()
    let response = await fetch(("/api/breeds?" + "page=" + current_page + "&per_page=" + per_page + "&name=" + name), {
        headers: {
            "OnPage": "true"
        }
    });
    if(response.ok){
        response = await response.json();
        breeds = response.breeds;
        let count = response.number
        count_breeds = count
        pages_length = Math.ceil(count / 5)
        console.log(pages_length)
        pages = []
        for (let i = 1; i <= pages_length; i++) {
            page = {};
            page.number = i;
            page.is_active = "";
            pages.push(page);
        }
        console.log(pages)
        pages[current_page - 1].is_active = "active";
        pre_dis = "";
        ne_dis = "";
        if (pages[0].is_active === "active") pre_dis = "disabled";
        if (pages[pages_length - 1].is_active === "active") ne_dis = "disabled";
        let table = await fetch("/javascripts/templates/breeds_table.mst")
        table = await table.text()
        document.getElementById('cent').innerHTML=inn
        const renderedHtmlStr = Mustache.render(resp, { pages, pre_dis, ne_dis });
        document.getElementById('pag').innerHTML = renderedHtmlStr
        const renderedHtmlStr2 = Mustache.render(table, { breeds});
        document.getElementById('breeds-table').innerHTML = renderedHtmlStr2
        element.removeAttribute("disabled");
    }
    else{
        let resp2 = await fetch("/javascripts/templates/errors/error_500.mst");
        resp2 = await resp2.text();
        document.getElementById("center").innerHTML = resp2;
    }
};


window.addEventListener('load', async (le) => {
    try {
        current_page = 1;
        await render(5);
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }
});
async function pageFunction(number) {
    try {
        current_page = number;
        await render(5);
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }

};

async function prevFunction() {
    try {
        if (current_page > 1) {
            current_page -= 1;
            await render(5);
        }
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }

};
async function nextFunction() {
    try {
        if (current_page < pages_length) {
            current_page += 1;
            await render(5);
        }
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }

};

document.getElementById('search-breed').addEventListener("change", async function () {
    try{
        current_page = 1;
        name = document.getElementById('search-breed').value;
        await render(5);
    }
    catch(err){
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err);
    }
});