async function delFunction(){
    try {
        let loading = await fetch("/javascripts/templates/loading_but.mst")
        loading = await loading.text()
        document.getElementById('del').innerHTML=loading
        document.getElementById('del').setAttribute("disabled",true);
        let id = document.getElementById("breedId").value
        let response = await fetch(window.location.pathname, {
            method: "POST",
            params:{
                id:id
            }
        });
        console.log(response)
        console.log(response.status)
        response=await response.json()
        console.log(response.id )
        if (response.id === id) {
            breeds = JSON.parse(localStorage.getItem("breeds"))
            for(let i=0;i<breeds.length;i++)
            {
                if (breeds[i].id===id){
                    breeds.splice(i, 1)
                    localStorage.setItem("breeds", JSON.stringify(breeds));
                }
            }
            let resp2 = await fetch("/javascripts/templates/deleted_breed.html")
            resp2 = await resp2.text()
            document.getElementById("del-body").innerHTML=resp2
            $('#deleteModal').modal('hide');
        }
        else{
            if (response.status === 500) {
                let resp2 = await fetch("/javascripts/templates/errors/error_500.html")
                resp2 = await resp2.text()
                document.getElementById("del-body").innerHTML=resp2
                $('#deleteModal').modal('hide');
            }
            else{
                let resp2 = await fetch("/javascripts/templates/errors/error_404.html")
                resp2 = await resp2.text()
                document.getElementById("del-body").innerHTML=resp2
                $('#deleteModal').modal('hide');
            }
            console.log("No, not deleted")
        }
    }
    catch (err) {
        document.getElementById("del-body").innerHTML="Something broke on client!"
        console.log(err)
    }
}; 