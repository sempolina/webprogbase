//localStorage.setItem("cats","[]");

document.getElementById('addition').addEventListener('submit', async (e) => {
  try {
    e.preventDefault()
    let body = new FormData(document.getElementById('addition'))
    let inn = document.getElementById('addition').innerHTML
    let loading = await fetch("/javascripts/templates/loading.mst")
    loading = await loading.text()
    document.getElementById('addition').innerHTML = loading
    let response = await fetch("/api/breeds", {
      method: "POST",
      body: body
    });
    if (response.ok) {
      let breeds = []
      response = await response.json()
      breeds = JSON.parse(localStorage.getItem("breeds"))
      if (breeds.length === 3) {
        breeds.splice(0, 1)
      };
      response.add_date = response.add_date.toString();
      console.log(breeds);
      breeds.push(response);
      // console.log(breeds);
      localStorage.setItem("breeds", JSON.stringify(breeds));
      console.log(response)
      let resp2 = await fetch("/javascripts/templates/new_breed.mst")
      resp2 = await resp2.text()
      console.log(resp2)
      const renderedHtmlStr = Mustache.render(resp2, { breeds });
      document.getElementById('addition').innerHTML = inn
      document.getElementById('last breeds').innerHTML = renderedHtmlStr
    }
    else {
      let resp2 = await fetch("/javascripts/templates/errors/error_500.mst")
      resp2 = await resp2.text()
      document.getElementById("cent").innerHTML=resp2
    }
  }
  catch (err) {
    document.getElementById("cent").innerHTML = "Something broke on client!"
    console.log(err)
  }
});
window.addEventListener('load', async (le) => {
  try {
    let resp2 = await fetch("/javascripts/templates/new_breed.mst")
    resp2 = await resp2.text()

    if (localStorage.getItem("breeds")) {
      breeds = JSON.parse(localStorage.getItem("breeds"))
    }
    else {
      breeds = []
      localStorage.setItem("breeds", JSON.stringify([]));
    }
    //breeds = JSON.parse(localStorage.getItem("breeds"))
    console.log(breeds)
    console.log(breeds[0])
    console.log(localStorage.getItem("breeds"));
    const renderedHtmlStr = Mustache.render(resp2, { breeds });
    console.log(renderedHtmlStr)
    document.getElementById('last breeds').innerHTML = renderedHtmlStr
  }
  catch(err){
    document.getElementById("cent").innerHTML = "Something broke on client!"
    console.log(err)
  }
});
