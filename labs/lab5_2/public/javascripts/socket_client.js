const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation);
console.log(`${protocol}//${location.host}`)
console.log("Hello from web socket");
async function showToastMessage(messageText) {
    try{
        let toastEl = await fetch("/javascripts/templates/toast.mst")
        toastEl = await toastEl.text()
        item=messageText
        const renderedHtmlStr = Mustache.render(toastEl, { item});
        const toastOption = {
            animation: true,
            autohide: true,
            delay: 20000,
        };
        document.getElementById("cont").innerHTML=document.getElementById("cont").innerHTML+renderedHtmlStr
        // const toast1 = new bootstrap.Toast(renderedHtmlStr, toastOption);
        // console.log(toast1)
        $("#toast"+messageText.id).toast(toastOption);
        $("#toast"+messageText.id).toast('show');
    }
    catch (err){
        console.log(err)
    }
}

connection.addEventListener('open', () => console.log(`Connected to ws server`));
connection.addEventListener('error', () => console.error(`ws error`));
connection.addEventListener('message',
async (message) => {
    await showToastMessage(JSON.parse(message.data));
    console.log(`ws message: ${JSON.parse(message.data)}`);
});

connection.addEventListener('close', () => console.log(`Disconnected from ws server`));
