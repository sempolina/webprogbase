//localStorage.setItem("cats","[]");

document.getElementById('addition').addEventListener('submit', async (e) => {
  try {
    e.preventDefault()
    let body = new FormData(document.getElementById('addition'))
    let inn = document.getElementById('addition').innerHTML
    let loading = await fetch("/javascripts/templates/loading.mst")
    loading = await loading.text()
    document.getElementById('addition').innerHTML = loading
    let response = await fetch("/api/cats", {
      method: "POST",
      body: body
    });
    if (response.ok) {
      console.log("response")
      response = await response.json()
      cats = JSON.parse(localStorage.getItem("cats"))
      if (cats.length === 3) {
        cats.splice(0, 1)
      };
      response.add_date = response.add_date.toString();
      console.log(cats);
      cats.push(response);
      // console.log(cats);
      localStorage.setItem("cats", JSON.stringify(cats));
      console.log(response)
      let resp2 = await fetch("/javascripts/templates/new_cat.mst")
      resp2 = await resp2.text()
      console.log(resp2)
      const renderedHtmlStr = Mustache.render(resp2, { cats });
      document.getElementById('addition').innerHTML = inn
      document.getElementById('last cats').innerHTML = renderedHtmlStr
    }
    else {
      let resp2 = await fetch("/javascripts/templates/errors/error_500.mst");
      resp2 = await resp2.text();
      document.getElementById("cent").innerHTML = resp2;
    }
  }
  catch (err) {
    document.getElementById("cent").innerHTML = "Something broke on client!"
    console.log(err)
  }
});
window.addEventListener('load', async (le) => {
  try {
    let resp2 = await fetch("/javascripts/templates/new_cat.mst")
    resp2 = await resp2.text()

    if (localStorage.getItem("cats")) {
      cats = JSON.parse(localStorage.getItem("cats"))
    }
    else {
      cats = []
      localStorage.setItem("cats", JSON.stringify([]));
    }
    //cats = JSON.parse(localStorage.getItem("cats"))
    console.log(cats[0])
    console.log(cats);
    // console.log(localStorage.getItem("cats"));
    const renderedHtmlStr = Mustache.render(resp2, { cats });
    console.log(renderedHtmlStr)
    document.getElementById('last cats').innerHTML = renderedHtmlStr
  }
  catch (err) {
    document.getElementById("cent").innerHTML = "Something broke on client!"
    console.log(err)
  }

});

//2020-10-08T11:22:42.508Z