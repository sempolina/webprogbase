current_page = 0;
name = ""
pages_length = 0
async function render(per_page) {
    let element = document.getElementById("search-cat");
    element.setAttribute("disabled", true);
    document.getElementById('pag').innerHTML = ""
    document.getElementById('cats-table').innerHTML = ""
    let inn = document.getElementById('cent').innerHTML
    let loading = await fetch("/javascripts/templates/loading.mst")
    let resp = await fetch("/javascripts/templates/pagination.mst")
    if (loading && resp) {
        loading = await loading.text()
        document.getElementById('cent').innerHTML = loading
        resp = await resp.text()
        let response = await fetch(("/api/cats?" + "page=" + current_page + "&per_page=" + per_page + "&name=" + name), {
            headers: {
                "OnPage": "true"
            }
        });
        if (response.ok) {
            response = await response.json();
            cats = response.cats;
            let count = response.number
            count_cats = count
            console.log(count)
            pages_length = Math.ceil(count / 5)
            console.log(pages_length)
            pages = []
            for (let i = 1; i <= pages_length; i++) {
                page = {};
                page.number = i;
                page.is_active = "";
                pages.push(page);
            }
            console.log(pages)
            pages[current_page - 1].is_active = "active";
            pre_dis = "";
            ne_dis = "";
            if (pages[0].is_active === "active") pre_dis = "disabled";
            if (pages[pages_length - 1].is_active === "active") ne_dis = "disabled";

            let table = await fetch("/javascripts/templates/cats_table.mst")
            table = await table.text()
            console.log(cats)
            console.log(table)
            document.getElementById('cent').innerHTML = inn
            const renderedHtmlStr = Mustache.render(resp, { pages, pre_dis, ne_dis });
            document.getElementById('pag').innerHTML = renderedHtmlStr
            const renderedHtmlStr2 = Mustache.render(table, { cats });
            document.getElementById('cats-table').innerHTML = renderedHtmlStr2
            element.removeAttribute("disabled");
        }
        else{
            let resp2 = await fetch("/javascripts/templates/errors/error_500.mst");
            resp2 = await resp2.text();
            document.getElementById("center").innerHTML = resp2;
        }
    }
    else {
        let resp2 = await fetch("/javascripts/templates/errors/error_500.mst");
        resp2 = await resp2.text();
        document.getElementById("center").innerHTML = resp2;
    }

};

window.addEventListener('load', async (le) => {
    try {
        current_page = 1;
        await render(5);
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }
});
async function pageFunction(number) {
    try {
        current_page = number;
        await render(5);
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }

};

async function prevFunction() {
    try {
        if (current_page > 1) {
            current_page -= 1;
            await render(5);
        }
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }

};
async function nextFunction() {
    try {
        if (current_page < pages_length) {
            current_page += 1;
            await render(5);
        }
    }
    catch (err) {
        document.getElementById("center").innerHTML = "Something broke on client!"
        console.log(err)
    }

};

document.getElementById('search-cat').addEventListener("change", async function () {
    try {
        current_page = 1;
        name = document.getElementById('search-cat').value;
        await render(5);
    }
    catch (err) {
        document.getElementById("center").innerHTML ="Something broke on client!";
        console.log(err);
    }
});