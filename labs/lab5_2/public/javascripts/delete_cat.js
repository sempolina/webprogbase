async function delFunction() {
    try {
        let loading = await fetch("/javascripts/templates/loading_but.mst")
        loading = await loading.text()
        document.getElementById('del').innerHTML = loading
        document.getElementById('del').setAttribute("disabled", true);
        let id = document.getElementById("catId").value
        let response = await fetch(window.location.pathname, {
            method: "POST",
            params: {
                id: id
            }
        });
        console.log(response)
        console.log(response.status)
        if (response.ok) {
            response = await response.json()
            cats = JSON.parse(localStorage.getItem("cats"))
            for (let i = 0; i < cats.length; i++) {
                if (cats[i].id === id) {
                    cats.splice(i, 1)
                    localStorage.setItem("cats", JSON.stringify(cats));
                }
            }
            let resp2 = await fetch("/javascripts/templates/deleted_cat.html")
            resp2 = await resp2.text()
            document.getElementById("del-body").innerHTML = resp2
            $('#deleteModal').modal('hide');
        }
        else {
            if (response.status === 500) {
                let resp2 = await fetch("/javascripts/templates/errors/error_500.mst")
                resp2 = await resp2.text()
                document.getElementById("del-body").innerHTML=resp2
                $('#deleteModal').modal('hide');
            }
            else{
                let resp2 = await fetch("/javascripts/templates/errors/error_404.mst")
                resp2 = await resp2.text()
                document.getElementById("del-body").innerHTML=resp2
                $('#deleteModal').modal('hide');
            }
            console.log("No, not deleted")
        }
    }
    catch (err) {
        document.getElementById("del-body").innerHTML="Something broke on client!"
        console.log(err)
    }
}; 