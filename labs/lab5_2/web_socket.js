const WebSocketServer = require('ws').Server;

class WsServer {
    constructor(server) {
        this.connections = [];
        this.wsServer = new WebSocketServer({
            server: server
        });
        this.wsServer.on('connection', (connection) => {
            console.log("On connection")
            this.addConnection(connection);
            connection.send("[Server]: Welcome!");
            connection.on('message', (message) => this.notifyAll(message.toString()));
            connection.on('close', () => this.removeConnection(connection));
        });
    }
 
    addConnection(connection) { this.connections.push(connection); }
 
    removeConnection(connection) {     this.connections.splice(this.connections.indexOf(connection), 1); }
 
    notifyAll(message) {
        console.log("notify to connections: ", this.connections.length);
        for (const connection of this.connections) {
          connection.send(JSON.stringify(message));
        }
    }
 };
  
 module.exports = WsServer;
