
const BreedRepository = require('../repositories/breedRepository');
const CatRepository = require('../repositories/catRepository');
const MediaRepository = require('../repositories/mediaRepository');
const fs = require('fs').promises;
const mustache = require("mustache");
const Breed = require('../models/breed');

const breedProperty = Symbol('breed');
const config = require('../config');

const { db: { host, port, name } } = config.dev;
const connectionString = `mongodb+srv://${host}:${port}/${name}?retryWrites=true&w=majority`;
console.log(connectionString);

repos_c = new CatRepository(connectionString)

repository = new BreedRepository(connectionString)
repo_m = new MediaRepository()

//
// … @todo setup app here
//
notifyAll = require('../bin/www');

module.exports = {
    async getBreeds(req, res, next) {
        try {
            // console.log(req.query.page, req.query.per_page, req.query.name, req.query.next)
            // console.log(req.headers)
            // console.log(req.headers.onpage)
            if (req.headers.onpage) {
                console.log("OnPage")
                const breeds = await repository.getBreeds();
                let breeds2 = [];
                if (req.query.name) {
                    for (let i = 0; i < breeds.length; i++) {
                        if (breeds[i].name.includes(req.query.name)) {
                            breeds2.push(breeds[i]);
                        }
                    }
                }
                else breeds2 = breeds;
                if (!req.query.page) req.query.page = 1;
                let breeds3 = [];
                if (!('per_page' in req.query)) req.query.per_page = 1
                if (req.query.next && (Math.ceil(breeds2.length / req.query.per_page) != req.query.page)) req.query.page++;
                if (req.query.prev && (1 != req.query.page)) req.query.page--;
                if (req.query.per_page > 100) req.query.per_page = 100
                const first = req.query.per_page * (req.query.page - 1)
                const len = Math.min(breeds2.length, req.query.page * req.query.per_page)
                for (let i = first; i < len; i++) {
                    breeds3.push(breeds2[i])
                }
                req[breedProperty] = breeds3;
                Data = {}
                Data.breeds = breeds3;
                console.log(breeds3);
                Data.number = breeds2.length
                res.send(Data)
            }
            else {
                const breeds = await repository.getBreeds();
                const resultsArray = await Promise.all([
                    fs.readFile("./views/partials/head.mst"),
                    fs.readFile("./views/partials/header.mst"),
                    fs.readFile("./views/partials/footer.mst"),
                    fs.readFile("./views/breeds.mst")
                ])
                const head = resultsArray[0].toString();
                const header = resultsArray[1].toString();
                const footer = resultsArray[2].toString();
                const userTemplate = resultsArray[3].toString();
                let Data = {
                    links: [{
                        l: "/",
                        name: "Home"
                    },
                    {
                        l: "/api/cats",
                        name: "Cats"
                    },
                    {
                        l: "",
                        name: "Breeds"
                    },
                    {
                        l: "/api/users",
                        name: "Users"
                    },
                    {
                        l: "/api/about",
                        name: "About"
                    }
                    ],
                    count: breeds.length
                };
                console.log(Data)
                // Data.count = Math.max(Math.ceil(cats.length / req.query.per_page), 1)
                //console.log(await cats3[0].populate("id_breed"))
                const partials = {
                    head: head,
                    header: header,
                    footer: footer
                };
                res.send(mustache.render(userTemplate, Data, partials))
            }

        }
        catch (err) {
            return await next(err);
        }

    },
    async getBreedById(req, res, next) {
        try {
            const breed = await repository.getBreedById(req.params.id);
            if (breed) {
                req[breedProperty] = breed;
                console.log(req[breedProperty]);
                const resultsArray = await Promise.all([
                    fs.readFile("./views/partials/head.mst"),
                    fs.readFile("./views/partials/header.mst"),
                    fs.readFile("./views/partials/footer.mst"),
                    fs.readFile("./views/breed.mst")
                ])
                const head = resultsArray[0].toString();
                const header = resultsArray[1].toString();
                const footer = resultsArray[2].toString();
                const userTemplate = resultsArray[3].toString();
                let Data = {
                    links: [{
                        l: "/",
                        name: "Home"
                    },
                    {
                        l: "/api/cats",
                        name: "Cats"
                    },
                    {
                        l: "/api/breeds",
                        name: "Breeds"
                    },
                    {
                        l: "/api/users",
                        name: "Users"
                    },
                    {
                        l: "/api/about",
                        name: "About"
                    }
                    ]
                };
                Data.breed = breed;
                const partials = {
                    head: head,
                    header: header,
                    footer: footer
                };
                res.send(mustache.render(userTemplate, Data, partials))
                //res.send(req[catProperty]);
            }
            else {
                res.sendStatus(404);
            }
        }
        catch (err) {
            await next(err);
        }

    },
    async getBreedHandler(req, res, next, next2) {
        const breed = await repository.getBreedById(req.params.id);
        if (breed) {
            req[breedProperty] = breed;
            console.log(req[breedProperty]);
            await next(next2);
        }
        else {
            res.sendStatus(404);
        }
    },
    async deleteBreedbyId(req, res, next) {
        try {
            const cats = await repos_c.getCats();
            let cats2 = []

            for (let i = 0; i < cats.length; i++) {

                if (cats[i].id_breed._id == req.params.id) {
                    cats2.push(cats[i]);
                }
            }
            for (let i = 0; i < cats2.length; i++) {
                await repos_c.deleteCat(cats2[i]._id);
            }
            await repository.deleteBreed(req.params.id);
            // res.send(req[breedProperty])
            id = req.params.id
            res.send({ id })
        }
        catch (err) {
            await next(err);
        }
    },
    async addBreed(req, res, next) {
        //console.log(req.files);
        console.log("Breed add")
        try {
            let breed = {};
            console.log(req.body)
            breed.name = req.body.name;
            breed.size = req.body.size;
            breed.age_of_life = req.body.age_of_life;
            const o = await repo_m.uploadRaw(req.files['avaUrl'].data);
            breed.avaUrl = o.url;
            const id = await repository.addBreed(breed);
            breed.id = id;
            breed.url = '/api/breeds/' + id
            breed.add_date = new Date(Date.now())
            req[breedProperty] = breed;
            notifyAll(breed);
            res.send(req[breedProperty])

        }
        catch (err) {
            await next(err);
        }

    },
    async updateBreed(req, res, next) {
        try {
            const breed = repository.getBreedById(req.params.id);
            console.log(req.files);
            fs.writeFileSync('./returned/breed.json', req.files['breed'].data);
            const jsonText = fs.readFileSync('./returned/breed.json');
            const jsonArray = JSON.parse(jsonText);
            if ('name' in jsonArray) breed.name = jsonArray.name;
            if ('breed' in jsonArray) cat.breed = jsonArray.breed;
            if ('atAge' in jsonArray) cat.atAge = jsonArray.atAge;
            if ('weight' in jsonArray) cat.weight = jsonArray.weight;
            if ('registeredAt' in jsonArray) cat.registeredAt = jsonArray.registeredAt;
            await repository.updateCat(cat);
            req[catProperty] = cat;
            res.send(req[catProperty])
        }
        catch (err) {
            await next(err);
        }
    },
    async new(req, res, next) {
        console.log("New");
        try {
            const resultsArray = await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/new2.mst")
            ])
            const head = resultsArray[0].toString();
            const header = resultsArray[1].toString();
            const footer = resultsArray[2].toString();
            const userTemplate = resultsArray[3].toString();

            const Data = {
                links: [{
                    l: "/",
                    name: "Home"
                },
                {
                    l: "/api/cats",
                    name: "Cats"
                },
                {
                    l: "/api/breeds",
                    name: "Breeds"
                },
                {
                    l: "/api/users",
                    name: "Users"
                },
                {
                    l: "/api/about",
                    name: "About"
                }
                ]
            };
            const partials = {
                head: head,
                header: header,
                footer: footer
            };
            res.send(mustache.render(userTemplate, Data, partials))
        }
        catch (err) {
            await next(err);
        }
    },
    async ErrorHandler(err, req, res, next) {
        console.error(err.stack);
        res.status(500).send('Something broke!');
    }

};