
const UserRepository = require('../repositories/userRepository');

const userProperty = Symbol('user');
const catProperty = Symbol('cat');

const config = require('../config');
 

const { db: { host, port, name } } = config.dev;
const connectionString = `mongodb+srv://${host}:${port}/${name}?retryWrites=true&w=majority`;

rip = new UserRepository(connectionString);
const fs = require("fs").promises;
const mustache = require("mustache");

module.exports = {
    async get_Users(req, res,next) {
        try{
            if (!('per_page' in req.query)) req.query.per_page = 10
        if (req.query.per_page > 100) req.query.per_page = 100
        const users = await rip.getUsers();
        if (('page' in req.query) && ('per_page' in req.query)) {
            const first = req.query.per_page * (req.query.page - 1)
            let users2 = [];
            const len = Math.min(users.length, req.query.page * req.query.per_page)
            for (let i = first; i < len; i++) {
                users2.push(users[i])
            }
            req[userProperty] = users2;
        }
        else req[userProperty] = users;
        console.log(req[userProperty]);
        const resultsArray = await Promise.all([
            fs.readFile("./views/partials/head.mst"),
            fs.readFile("./views/partials/header.mst"),
            fs.readFile("./views/partials/footer.mst"),
            fs.readFile("./views/users.mst")
        ])  
        const head = resultsArray[0].toString();
        const header = resultsArray[1].toString();
        const footer = resultsArray[2].toString();
        const userTemplate = resultsArray[3].toString();
        let Data = {
            links: [{
                l: "/",
                name: "Home"
            },
            {
                l: "/api/cats",
                name: "Cats"
            },
            {
                l: "/api/breeds",
                name: "Breeds"
            },
            {
                l: "",
                name: "Users"
            },
            {
                l: "/api/about",
                name: "About"
            }
            ]
        };
        Data.users = users;
        const partials = {
            head: head,
            header: header,
            footer: footer
        };
        res.send(mustache.render(userTemplate, Data, partials))
        //res.send(req[userProperty]);
        }
        catch (err){
            return next(err);
        }
    },

    async get_UserById(req, res,next) {
        try{
            const user = await rip.getUserById(req.params.id);
        if (user) {
            req[userProperty] = user;
            console.log(req[userProperty]);
            const resultsArray = await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/user.mst")
            ])  
            const head = resultsArray[0].toString();
            const header = resultsArray[1].toString();
            const footer = resultsArray[2].toString();
            const userTemplate = resultsArray[3].toString();
            let Data = {
                links: [{
                    l: "/",
                    name: "Home"
                },
                {
                    l: "/api/cats",
                    name: "Cats"
                },
                {
                    l: "/api/breeds",
                    name: "Breeds"
                },
                {
                    l: "/api/users",
                    name: "Users"
                },
                {
                    l: "/api/about",
                    name: "About"
                }
                ]
            };
            Data.user = user;
            const partials = {
                head: head,
                header: header,
                footer: footer
            };
            res.send(mustache.render(userTemplate, Data, partials))
            //res.send(req[userProperty]);
        }
        else {
            res.sendStatus(404);
        }
        }
        catch (err) {
            await next(err);
        }
    },
    async ErrorHandler(err,req, res,next){
        console.error(err.stack);
        res.status(500).send('Something broke!');
    }
};
