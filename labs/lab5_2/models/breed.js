/**
 * @typedef Breed
 * @property {string} id.required - unique id
 * @property {string} name.required - name of breed
 * @property {string} size.required - size of breed
 * @property {number} age_of_life.required - avarage age of life
 * @property {string} avaUrl.required - ava of breed
 */
class Breed {

    constructor(id, name,size,age_of_life, avaUrl) {
        this.id = id; 
        this.name = name;
        this.size=size;
        this.age_of_life = age_of_life; 
        this.avaUrl = avaUrl; //URL
    }
 };
 
 module.exports = Breed;