import Vue from 'vue'
import App from './App.vue'
import { createProvider } from './vue-apollo'
import router from './router'
import Vuex from 'vuex'
 
Vue.use(Vuex)

Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    jwt: null
  },
  mutations: {
    set(state,jwt_new) {
      state.jwt=jwt_new;
    },
    delete(state){
      state.jwt=null;
    }
  }
})


new Vue({
  store: store,
  apolloProvider: createProvider(),
  router,
  render: h => h(App)
}).$mount('#app')
