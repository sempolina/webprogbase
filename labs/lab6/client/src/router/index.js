import Vue from 'vue'
import VueRouter from 'vue-router'
import sign from '../views/Sign.vue'
import register from '../views/Register.vue'
import chats from '../views/Chats.vue'
Vue.use(VueRouter)

const ifAuth= function(to,from,next){
  if(localStorage.getItem('username')){
    next();
  }
  else{
    router.push('/sign')
  }
}

const routes = [
  {
    path: '/sign',
    name: 'Sign',
    component: sign
  },
  {
    path: '/register',
    name: 'Register',
    component: register
  },
  {
    path: '/',
    name: 'Chats',
    component: chats,
    beforeEnter: ifAuth
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
