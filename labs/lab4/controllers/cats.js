
const CatRepository = require('../repositories/catRepository');
const BreedRepository = require('../repositories/breedRepository');
const UserRepository = require('../repositories/userRepository');

const fs = require('fs').promises;
const mustache = require("mustache");

const Cat = require('../models/cat');

const catProperty = Symbol('cat');

const config = require('../config');


const { db: { host, port, name } } = config.dev;
const connectionString = `mongodb+srv://${host}:${port}/${name}?retryWrites=true&w=majority`;

repos = new CatRepository(connectionString)
repos_b = new BreedRepository( connectionString)
repos_u = new UserRepository( connectionString)

module.exports = {
    async getCats(req, res, next) {
        try {
            console.log(req.query.page, req.query.per_page, req.query.name, req.query.next)
            const cats = await repos.getCats();
            let cats2 = [];
            if (req.query.name) {
                for (let i = 0; i < cats.length; i++) {
                    if (cats[i].name.includes(req.query.name)) {
                        console.log(cats[i]);
                        cats2.push(cats[i]);
                    }
                }
            }
            else cats2 = cats;
            if (!req.query.page) req.query.page = 1;
            let cats3 = [];
            if (!('per_page' in req.query)) req.query.per_page = 1
            if (req.query.next && (Math.ceil(cats2.length / req.query.per_page) != req.query.page)) req.query.page++;
            if (req.query.prev && (1 != req.query.page)) req.query.page--;
            if (req.query.per_page > 100) req.query.per_page = 100
            if (('page' in req.query) && ('per_page' in req.query)) {
                const first = req.query.per_page * (req.query.page - 1)
                const len = Math.min(cats2.length, req.query.page * req.query.per_page)
                for (let i = first; i < len; i++) {
                    cats2[i].id_breed.id=cats2[i].id_breed._id.toString();
                    cats3.push(cats2[i])
                }
                req[catProperty] = cats3;
            }
            else cats3 = cats2;
            const resultsArray = await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/cats.mst")
            ])
            const head = resultsArray[0].toString();
            const header = resultsArray[1].toString();
            const footer = resultsArray[2].toString();
            const userTemplate = resultsArray[3].toString();
            let Data = {
                links: [{
                    l: "/",
                    name: "Home"
                },
                {
                    l: "",
                    name: "Cats"
                },
                {
                    l: "/api/breeds",
                    name: "Breeds"
                },
                {
                    l: "/api/users",
                    name: "Users"
                },
                {
                    l: "/api/about",
                    name: "About"
                }
                ],
                page:
                {
                    name: req.query.name,
                    current: req.query.page,
                    number: Math.max(Math.ceil(cats2.length / req.query.per_page), 1)
                }
            };
            Data.cats = cats3;
            //console.log(await cats3[0].populate("id_breed"))
            const partials = {
                head: head,
                header: header,
                footer: footer
            };
            res.send(mustache.render(userTemplate, Data, partials))
            console.log(req[catProperty]);
        }
        catch (err) {
            return  await next(err);
        }

    },
    async getCatById(req, res, next) {
        try {
            const cat = await repos.getCatById(req.params.id);
            console.log("Hello2!")
            if (cat) {
                req[catProperty] = cat;
                console.log(req[catProperty]);
                const resultsArray = await Promise.all([
                    fs.readFile("./views/partials/head.mst"),
                    fs.readFile("./views/partials/header.mst"),
                    fs.readFile("./views/partials/footer.mst"),
                    fs.readFile("./views/cat.mst")
                ])
                const head = resultsArray[0].toString();
                const header = resultsArray[1].toString();
                const footer = resultsArray[2].toString();
                const userTemplate = resultsArray[3].toString();
                let Data = {
                    links: [{
                        l: "/",
                        name: "Home"
                    },
                    {
                        l: "/api/cats",
                        name: "Cats"
                    },
                    {
                        l: "/api/breeds",
                        name: "Breeds"
                    },
                    {
                        l: "/api/users",
                        name: "Users"
                    },
                    {
                        l: "/api/about",
                        name: "About"
                    }
                    ]
                };
                Data.cat = cat;
                const partials = {
                    head: head,
                    header: header,
                    footer: footer
                };
                res.send(mustache.render(userTemplate, Data, partials))
                //res.send(req[catProperty]);
            }
            else {
                res.sendStatus(404);
            }
        }
        catch (err) {
            await next(err);
        }

    },
    async getCatHandler(req, res, next,next2) {
        const cat = await repos.getCatById(req.params.id);
        if (cat) {
            req[catProperty] = cat;
            console.log(req[catProperty]);
            await next(next2);
        }
        else {
            res.sendStatus(404);
        }
    },
    async deleteCatbyId(req, res, next) {
        try {
            await repos.deleteCat(req.params.id);
            res.send(req[catProperty])
        }
        catch (err) {
            await next(err);
        }
    },
    async addCat(req, res, next) {
        //console.log(req.files);
        try {
            let cat = {};
            cat.name = req.body.name;
            cat.id_breed = req.body.id_breed;
            cat.id_user = req.body.id_user;
            cat.atAge = req.body.age;
            cat.weight = req.body.weight;
            cat.registeredAt = req.body.register;
            const o = await repo_m.uploadRaw(req.files['avaUrl'].data);
            cat.avaUrl=o.url;
            if (!('name' in cat)  || !('atAge' in cat) ||
                !('weight' in cat) || !('registeredAt' in cat)) {
                res.sendStatus(400);
            }
            else {
                const id = await repos.addCat(cat);
                cat.id = id;
                console.log(id);
                req[catProperty] = cat;
                res.redirect('/api/cats/' + id);
            }
        }
        catch (err) {
            await next(err);
        }

    },
    async updateCat(req, res, next) {
        try {
            const cat =await  repos.getCatById(req.params.id);
            console.log(req.files);
            fs.writeFileSync('./returned/cat.json', req.files['cat'].data);
            const jsonText = fs.readFileSync('./returned/cat.json');
            const jsonArray = JSON.parse(jsonText);
            if ('name' in jsonArray) cat.name = jsonArray.name;
            if ('breed' in jsonArray) cat.breed = jsonArray.breed;
            if ('atAge' in jsonArray) cat.atAge = jsonArray.atAge;
            if ('weight' in jsonArray) cat.weight = jsonArray.weight;
            if ('registeredAt' in jsonArray) cat.registeredAt = jsonArray.registeredAt;
            await repos.updateCat(cat);
            req[catProperty] = cat;
            res.send(req[catProperty])
        }
        catch (err) {
            await next(err);
        }
    },
    async new(req, res, next) {
        console.log("New");
        try {
            const users = await repos_u.getUsers();
            const breeds = await repos_b.getBreeds();
            const resultsArray = await Promise.all([
                fs.readFile("./views/partials/head.mst"),
                fs.readFile("./views/partials/header.mst"),
                fs.readFile("./views/partials/footer.mst"),
                fs.readFile("./views/new.mst")
            ])
            const head = resultsArray[0].toString();
            const header = resultsArray[1].toString();
            const footer = resultsArray[2].toString();
            const userTemplate = resultsArray[3].toString();

            const Data = {
                links: [{
                    l: "/",
                    name: "Home"
                },
                {
                    l: "/api/cats",
                    name: "Cats"
                },
                {
                    l: "/api/breeds",
                    name: "Breeds"
                },
                {
                    l: "/api/users",
                    name: "Users"
                },
                {
                    l: "/api/about",
                    name: "About"
                }
                ]
            };
            Data.users=users;
            Data.breeds=breeds;
            const partials = {
                head: head,
                header: header,
                footer: footer
            };
            res.send(mustache.render(userTemplate, Data, partials))
            
        }
        catch (err) {
            await next(err);
        }
    },
    async ErrorHandler(err,req, res,next){
        console.error(err.stack);
        res.status(500).send('Something broke!');
    }

};