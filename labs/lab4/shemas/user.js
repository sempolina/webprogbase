const mongoose = require('mongoose');
const UserSchema = new mongoose.Schema({
    login:{type:String,required:true},
    fullname:{type:String,required:true},
    role:{type:Number,required:true},
    registeredAt:{type:String,required:true},
    avaUrl:{type:String,required:true},
    bio:{type:String,required:true},
    isEnabled:{type:Boolean,required:true},
 }, { collection: 'users' });
const UserModel = mongoose.model('User',UserSchema);   
module.exports = UserModel;