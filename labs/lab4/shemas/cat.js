const mongoose = require('mongoose');
const CatSchema = new mongoose.Schema({
    name:{type:String,required:true},
    id_breed:{type: mongoose.Schema.ObjectId, ref: 'Breed'},
    id_user:{type: mongoose.Schema.ObjectId, ref: 'User'},
    atAge:{type:Number,required:true},
    weight:{type:Number,required:true},
    avaUrl:{type:String,required:true},
    registeredAt:{type:String,required:true}
 }, { collection: 'cats' });
const CatModel = mongoose.model('Cat',CatSchema); 
module.exports = CatModel;