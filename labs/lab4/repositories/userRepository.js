const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
const UserModel = require('../shemas/user');
class UserRepository {
    constructor(dbUrl) {       
        this.storage = new JsonStorage(dbUrl,UserModel);
    }
 
    async getUsers() { 
        await this.storage.connect();
        const result= await this.storage.getItems();
        await this.storage.disconnect();
        return result;
    }
 
    async getUserById(id) {
        await this.storage.connect();
        const result= await this.storage.getItemById(id);
        await this.storage.disconnect();
        return result;
    }
};
 
module.exports = UserRepository;
