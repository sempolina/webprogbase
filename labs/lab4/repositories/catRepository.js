const Cat = require('../models/cat');
const JsonStorage = require('../jsonStorage');
const mongoose = require('mongoose');
const fs = require('fs').promises;
const CatModel = require('../shemas/cat');
class CatRepository {
 
    constructor(dbUrl) {        
        this.storage = new JsonStorage(dbUrl,CatModel);
    }
 
    async getCats() { 
        await this.storage.connect();
        const result= await this.storage.getItems();
        await this.storage.disconnect();
        return result;
    }
 
    async getCatById(id) {
        await this.storage.connect();
        const result= await this.storage.getItemById(id);
        await this.storage.disconnect();
        return result;
    }
    async addCat(cat){
        await this.storage.connect();
        const id= await this.storage.addItem(cat);
        console.log(id);
        await this.storage.disconnect();
        return id;
    }
    async updateCat(cat){
        await this.storage.connect();
        await this.storage.updateItem(cat);
        await this.storage.disconnect();
    }
    async deleteCat(id){
        await this.storage.connect();
        await this.storage.deleteItem(id);
        await this.storage.disconnect();
    }
};
 
module.exports = CatRepository;
