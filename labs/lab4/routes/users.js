const express = require('express');
const router = express.Router();
const contoller=require('../controllers/users')

/**
 * We get all users
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} page.query - number of page
 * @param {integer} per_page.query - number of users on page
 * @returns {Array.<User>} 200 - page with users
 */
router.get('/', contoller.get_Users,contoller.ErrorHandler);
/**
 * We get user by his id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.get('/:id', contoller.get_UserById,contoller.ErrorHandler);

module.exports = router;
