const express = require('express');
const router = express.Router();
const contoller=require('../controllers/cats');
/**
 * We get all cats
 * @route GET /api/cats
 * @group Cats - cat operations
 * @param {integer} page.query - number of page
 * @param {integer} per_page.query - number of cats on page
 * @returns {Array.<Cat>} 200 - get page of Cats
 */
router.get("/", contoller.getCats,contoller.ErrorHandler);
/**
 * We form new cat
 * @route GET /api/casts/new
 * @group Cats - cat operations
 * @returns {url} 304 - Cat object
 * @returns {Error} 400 - Bad request
 */
router.get("/new", contoller.new,contoller.ErrorHandler);
/**
 * We add cat
 * @route POST /api/cats
 * @group Cats - cat operations
 * @param {file} cat.formData.required - file with cat
 * @returns {Cat.model} 201 - Cat object
 * @returns {Error} 400 - Bad request
 */
router.post('/', contoller.addCat,contoller.ErrorHandler);
/**
 * We get cat by his id
 * @route GET /api/cats/{id}
 * @group Cats - cat operations
 * @param {integer} id.path.required - id of the Cat - eg: 1
 * @returns {Cat.model} 200 - Cat object
 * @returns {Error} 404 - Cat not found
 */
router.get("/:id", contoller.getCatById,contoller.ErrorHandler);

/**
 * We update cat by his id
 * @route PUT /api/cats/{id}
 * @group Cats - cat operations
 * @param {integer} id.path.required - id of the Cat - eg: 1
 * @param {file} cat.formData.required- file with cat
 * @returns {Cat.model} 200 - Cat object
 * @returns {Error} 404 - Cat not found
 */
router.put("/:id",contoller.getCatHandler,contoller.updateCat,contoller.ErrorHandler);
/**
 * We delete cat by his id
 * @route DELETE /api/cats/{id}
 * @group Cats - cat operations
 * @param {integer} id.path.required - id of the Cat - eg: 1
 * @returns {Cat.model} 200 - Cat object
 * @returns {Error} 404 - Cat not found
 */
router.post("/:id", contoller.getCatHandler,contoller.deleteCatbyId,contoller.ErrorHandler);
module.exports = router;
