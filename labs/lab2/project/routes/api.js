const router = require('express').Router();

const userRouter = require('./users');
const catsRouter=require('./cats');
const mediaRouter=require('./media');

router.use('/users', userRouter);
router.use('/cats', catsRouter);
router.use('/media', mediaRouter);
module.exports = router;
