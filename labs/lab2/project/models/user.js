/**
 * @typedef User
 * @property {integer} id.required - unique id
 * @property {string} login.required - login of user
 * @property {string} fullname.required - fullname of user
 * @property {number} role.required - admin or not(0 or 1)
 * @property {string} registeredAt.required - date when user was registered
 * @property {string} avaUrl.required - ava of user
 * @property {boolean} isEnabled.required - if user is enabled
 */

class User {

    constructor(id, login, fullname,registeredAt,avaUrl,role=0,isEnabled=true) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;//number :0,1
        this.registeredAt = registeredAt; //date (string)
        this.avaUrl = avaUrl; //URL
        this.isEnabled = isEnabled; //boolean
    }
 };
 
 module.exports = User;
 