
const MediaRepository = require('../repositories/mediaRepository');

const imageProperty = Symbol('image');
const fs = require('fs');

repo = new MediaRepository('./data/media.json');

module.exports = {
    getImage(req,res){
        const media= repo.getMediaById(parseInt(req.params.id));
        if (media) {  
            console.log(__dirname+'/../data/media/'+media.filename);
            let file=fs.readFileSync(__dirname+'/../data/media/'+media.filename);
            res.send(Buffer.from(file));
        } 
        else {
            res.sendStatus(404);
        }
    },
    postImage(req,res){
        console.log(req.files);
        let name=req.files['media'].name.split('.');
        let media={};
        media.filename='cat'+repo.storage.nextId+'.'+name[1];
        console.log(media.filename);
        fs.writeFileSync('./data/media/'+media.filename, req.files['media'].data);
        id=repo.addMedia(media);
        res.status(201).send(id.toString());
    }
}