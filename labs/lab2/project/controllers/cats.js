
const CatRepository = require('../repositories/catRepository');
const fs = require('fs');

const catProperty = Symbol('cat');

repository= new CatRepository('./data/cats.json')

module.exports = {
    getCats(req, res) {
        console.log(req.query.page,req.query.per_page)
        const cats = repository.getCats();
        if(!('per_page'  in req.query))req.query.per_page=10
        if(req.query.per_page>100)req.query.per_page=100
            if(('page' in req.query )&& ('per_page'  in req.query)){
                const first=req.query.per_page*(req.query.page-1)
                let cats2=[];
                const len= Math.min(cats.length,req.query.page*req.query.per_page)
                for(let i=first;i<len;i++){
                    cats2.push(cats[i])
                }
                req[catProperty] = cats2;
            }
            else req[catProperty] = cats;
            console.log(req[catProperty]); 
            res.send(req[catProperty]); 

    },
    getCatById(req, res) {
        const cat= repository.getCatById(parseInt(req.params.id));
        if (cat) {
            req[catProperty] = cat;
            console.log(req[catProperty]);  
            res.send(req[catProperty]);
        } 
        else {
            res.sendStatus(404);
        }

    },
    getCatHandler(req,res,next){
        const cat= repository.getCatById(parseInt(req.params.id));
        if (cat) {
            req[catProperty] = cat;
            console.log(req[catProperty]);
            next();
        } 
        else {
            res.sendStatus(404);
        }
    },
    deleteCatbyId(req, res) {
        repository.deleteCat(parseInt(req.params.id));
        res.send(req[catProperty])
    },
    addCat(req,res){
        console.log(req.files);
        fs.writeFileSync('./returned/cat.json', req.files['cat'].data);
        const jsonText = fs.readFileSync('./returned/cat.json');
        const jsonArray = JSON.parse(jsonText);
        if(!('name' in jsonArray) || !('breed'in jsonArray)|| !('atAge'in jsonArray)||
        !('weight' in jsonArray)||!('registeredAt'in jsonArray))
        {
            res.sendStatus(400);
        }
        else{
            id=repository.addCat(jsonArray);
            jsonArray.id=id;
            req[catProperty] = jsonArray;
            res.statusCode = 201;
            res.setHeader("Location","http://localhost:3000/api/cats/"+id)
            res.send(req[catProperty])
        }
    },
    updateCat(req, res){
        const cat= repository.getCatById(parseInt(req.params.id));
        console.log(req.files);
        fs.writeFileSync('./returned/cat.json', req.files['cat'].data);
        const jsonText = fs.readFileSync('./returned/cat.json');
        const jsonArray = JSON.parse(jsonText);
        if('name' in jsonArray) cat.name=jsonArray.name;
        if('breed' in jsonArray) cat.breed=jsonArray.breed;
        if('atAge' in jsonArray) cat.atAge=jsonArray.atAge;
        if('weight' in jsonArray) cat.weight=jsonArray.weight;
        if('registeredAt' in jsonArray) cat.registeredAt=jsonArray.registeredAt;
        repository.updateCat(cat);
        req[catProperty] = cat;
        res.send(req[catProperty])
    }
};