
const UserRepository = require('../repositories/userRepository');

const userProperty = Symbol('user');

repos = new UserRepository('./data/users.json');

module.exports = {
    get_Users(req, res) {
        if(!('per_page'  in req.query))req.query.per_page=10
        if(req.query.per_page>100)req.query.per_page=100
        const users = repos.getUsers();
            if(('page' in req.query )&& ('per_page'  in req.query)){
                const first=req.query.per_page*(req.query.page-1)
                let users2=[];
                const len= Math.min(users.length,req.query.page*req.query.per_page)
                for(let i=first;i<len;i++){
                    users2.push(users[i])
                }
                req[userProperty] = users2;
            }
            else req[userProperty] = users;
            console.log(req[userProperty]); 
            res.send(req[userProperty]);
    },

    get_UserById(req, res) {
        const user = repos.getUserById(parseInt(req.params.id));
        if (user) {
            req[userProperty] = user;
            console.log(req[userProperty]);  
            res.send(req[userProperty]);
        } 
        else {
            res.sendStatus(404);
        }

    }
};
