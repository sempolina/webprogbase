
const readlineSync = require('readline-sync');

const UserRepository = require('./repositories/userRepository');
const CatRepository = require('./repositories/catRepository');
const Cat = require('./models/cat');

while (true)
{
    const input = readlineSync.question('\nEnter command:\n1. get/users\n2. get/users/{id}\n3. get/cats\n4. get/cats/{id}\n5. delete/cats/{id}\n6. update/cats/{id}\n7. post/cats\n');
    if (input.length === 0) break;
    const array=input.trim().split("/");
    const UserRepo= new UserRepository("./data/users.json");
    const CatRepo = new CatRepository("./data/cats.json");
    if(array[0]==='get')
    {
        if(array[1]==='users')
        {
            if(array.length===3&&(+array[2]))
            {
                let user=UserRepo.getUserById(+array[2]);
                if(user===null)
                {
                    console.log("Not found!")
                }
                else
                {
                    console.log("id:"+user.id+"\nlogin:"+user.login+"\nfullname:"+user.fullname+"\nrole:"+user.role+"\nregistered at:"+user.registeredAt+"\navaURL:"+user.avaUrl+"\nis Enabled:"+user.isEnabled);
                }
            }
            else if(array.length==2)
            {
                let users=UserRepo.getUsers();
                for(let i=0;i<users.length;i++)
                {
                    console.log((i+1)+". id:"+users[i].id+" login:"+users[i].login+" fullname:"+users[i].fullname);
                }
            }
            else{
                console.log("Command not found!");
            }
        }
        else if(array[1]==='cats')
        {
            if(array.length===3&&(+array[2]))
            {
                let cat=CatRepo.getCatById(+array[2]);
                if(cat===null)
                {
                    console.log("Not found!")
                }
                else
                {
                    console.log("id:"+cat.id +"\nname:"+cat.name+"\nbreed:"+cat.breed+"\nat age:"+cat.atAge+"\nweight:"+cat.weight+"\nregistered at:"+cat.registeredAt);
                }
            }
            else if(array.length==2)
            {
                let cats=CatRepo.getCats();
                for(let i=0;i<cats.length;i++)
                {
                    console.log((i+1)+". id:"+cats[i].id+" name:"+cats[i].name+" at age:"+cats[i].atAge);
                }
            }
            else{
                console.log("Command not found!");
            }
        }
        else {
            console.log("Command not found!");
        }
    }
    else if(array[0]==='update'&&array[1]==='cats'&&(+array[2])&&array.length===3){
        const name = readlineSync.question("Enter name: ");
        const breed = readlineSync.question("Enter breed: ");
        let atAge = readlineSync.question("Enter at which age: ");
        while(!(+(atAge.trim())))
        {
            atAge = readlineSync.question("Incorrect age, try again: ");
        }
        let weight = readlineSync.question("Enter weight: ");
        while(!(+(weight.trim())))
        {
            weight = readlineSync.question("Incorrect weight, try again: ");
        }
        const data= new Date(Date.now());
        let cat= new Cat(+array[2],name,breed,atAge,weight,data);
        const i=CatRepo.updateCat(cat);
        if(!i)
        {
            console.log("This item doesn`t exist!")
        }
    }
    else{
        if(array[0]==='delete'&&array[1]==='cats'&&(+array[2])&&array.length===3){
                const i=CatRepo.deleteCat(+array[2]);
                if(!i)
                {
                    console.log("This item doesn`t exist!")
                }
        }
        else if(array[0]==='post'&&array[1]==='cats'&&array.length===2)
        {
            const name = readlineSync.question("Enter name: ");
            const breed = readlineSync.question("Enter breed: ");
            let atAge = readlineSync.question("Enter at which age: ");
            while(!(+(atAge.trim())))
            {
                atAge = readlineSync.question("Incorrect age, try again: ");
            }
            let weight = readlineSync.question("Enter weight: ");
            while(!(+(weight.trim())))
            {
                weight = readlineSync.question("Incorrect weight, try again: ");
            }
            const data= new Date(Date.now());
            let cat= new Cat (0,name,breed,atAge,weight,data);
            CatRepo.addCat(cat);
        }
        else{
            console.log("Command not found!");
        }

    }
}
 
console.log('Bye.');
