class Cat {

    constructor(id, name,breed,atAge, weight,registeredAt) {
        this.id = id; 
        this.name = name;
        this.breed=breed;
        this.atAge = atAge; //at which age cat started live in the shelter
        this.weight = weight;
        this.registeredAt=registeredAt; //data
    }
 };
 
 module.exports = Cat;