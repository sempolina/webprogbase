class User {

    constructor(id, login, fullname,registeredAt,avaUrl,role=0,isEnabled=true) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;//number :0,1
        this.registeredAt = registeredAt; //data (string)
        this.avaUrl = avaUrl; //URL
        this.isEnabled = isEnabled; //boolean
    }
 };
 
 module.exports = User;
 