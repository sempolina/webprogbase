const express = require('express');
const router = express.Router();
const controller=require('../controllers/media')
/**
 * We download media by it`s id
 * @route GET /api/media/{id}
 * @group Media - media operations
 * @param {integer} id.path.required - id of the Media - eg: 1
 * @returns {file} 200 - file with media
 * @returns {Error} 404 - Cat not found
 */
router.get('/:id(\\d+)', controller.getImage);
/**
 * We add media
 * @route POST /api/media
 * @group Media - media operations
 * @param {file} media.formData.required - file with cat
 * @returns {integer} 201 - Media.id object
 */
router.post('/',controller.postImage)
module.exports = router;