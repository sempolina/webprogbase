/**
 * @typedef Media
 * @property {integer} id.required - unique id
 * @property {string} filename.required - name of image
 */
class Media {

    constructor(id,filename) {
        this.id = id; 
        this.filename=filename
    }
 };
 module.exports = Media;