/**
 * @typedef Cat
 * @property {integer} id.required - unique id
 * @property {string} name.required - name of cat
 * @property {string} breed.required - breed of cat
 * @property {number} atAge.required - at which age cat started live in the shelter
 * @property {number} weight.required - weight of cat
 * @property {string} registeredAt.required - date when cat was registered in the shelter
 */
class Cat {

    constructor(id, name,breed,atAge, avaUrl,weight,registeredAt) {
        this.id = id; 
        this.name = name;
        this.breed=breed;
        this.atAge = atAge; //at which age cat started live in the shelter
        this.avaUrl = avaUrl; //URL
        this.weight = weight;
        this.registeredAt=registeredAt; //data
    }
 };
 
 module.exports = Cat;