const Cat = require('../models/cat');
const JsonStorage = require('../jsonStorage');
class CatRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getCats() { 
        return this.storage.readItems();
    }
 
    getCatById(id) {
        const cats = this.storage.readItems();
        for (const item of cats) {
            if (item.id === id) {
                return new Cat(item.id, item.name,item.breed,item.atAge,item.avaUrl,item.weight, item.registeredAt);
            }
        }
        return null;
    }
    addCat(cat){
        cat.id=this.storage.nextId;
        let cats=this.storage.readItems();
        cats.push(cat);
        this.storage.writeItems(cats);
        return cat.id;
    }
    updateCat(cat){
        let cats = this.storage.readItems();
        for (let i=0;i<cats.length;i++) {
            if (cats[i].id === cat.id) {
                cats[i]=cat;
                this.storage.writeItems(cats);
                return true;
            }
        }
        return false;
    }
    deleteCat(id){
        let cats = this.storage.readItems();
        for (let i=0;i<cats.length;i++) {
            if (cats[i].id === id) {
                cats.splice(i,1);
                this.storage.writeItems(cats);
                return true
            }
        }
        return false;
    }
};
 
module.exports = CatRepository;
