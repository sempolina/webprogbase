
const CatRepository = require('../repositories/catRepository');
const fs = require('fs');
const mustache = require("mustache");
const { query } = require('express');
const Cat = require('../models/cat');


const catProperty = Symbol('cat');

repository = new CatRepository('./data/cats.json')

module.exports = {
    getCats(req, res) {
        console.log(req.query.page, req.query.per_page, req.query.name, req.query.next)
        const cats = repository.getCats();
        let cats2 = [];
        if (req.query.name) {
            for (let i = 0; i < cats.length; i++) {
                if (cats[i].name.includes(req.query.name)) {
                    console.log(cats[i]);
                    cats2.push(cats[i]);
                }
            }
        }
        else cats2 = cats;
        if (!req.query.page) req.query.page = 1;
        let cats3 = [];
        if (!('per_page' in req.query)) req.query.per_page = 1
        if (req.query.next && (Math.ceil(cats2.length / req.query.per_page) != req.query.page)) req.query.page++;
        if (req.query.prev && (1 != req.query.page)) req.query.page--;
        if (req.query.per_page > 100) req.query.per_page = 100
        if (('page' in req.query) && ('per_page' in req.query)) {
            const first = req.query.per_page * (req.query.page - 1)
            const len = Math.min(cats2.length, req.query.page * req.query.per_page)
            for (let i = first; i < len; i++) {
                cats3.push(cats2[i])
            }
            req[catProperty] = cats3;
        }
        else cats3 = cats2;
        const head = fs.readFileSync("./views/partials/head.mst").toString();
        const header = fs.readFileSync("./views/partials/header.mst").toString();
        const footer = fs.readFileSync("./views/partials/footer.mst").toString();
        const userTemplate = fs.readFileSync("./views/cats.mst").toString();

        let Data = {
            links: [{
                l: "/",
                name: "Home"
            },
            {
                l: "",
                name: "Cats"
            },
            {
                l: "/api/users",
                name: "Users"
            },
            {
                l: "/api/about",
                name: "About"
            }
            ],
            page:
            {
                name: req.query.name,
                current: req.query.page,
                number: Math.max(Math.ceil(cats2.length / req.query.per_page), 1)
            }
        };
        Data.cats = cats3;
        const partials = {
            head: head,
            header: header,
            footer: footer
        };
        res.send(mustache.render(userTemplate, Data, partials))
        console.log(req[catProperty]);
        //res.send(req[catProperty]);

    },
    getCatById(req, res) {
        const cat = repository.getCatById(parseInt(req.params.id));
        if (cat) {
            req[catProperty] = cat;
            console.log(req[catProperty]);
            const head = fs.readFileSync("./views/partials/head.mst").toString();
            const header = fs.readFileSync("./views/partials/header.mst").toString();
            const footer = fs.readFileSync("./views/partials/footer.mst").toString();
            const userTemplate = fs.readFileSync("./views/cat.mst").toString();
            let Data = {
                links: [{
                    l: "/",
                    name: "Home"
                },
                {
                    l: "/api/cats",
                    name: "Cats"
                },
                {
                    l: "/api/users",
                    name: "Users"
                },
                {
                    l: "/api/about",
                    name: "About"
                }
                ]
            };
            Data.cat = cat;
            const partials = {
                head: head,
                header: header,
                footer: footer
            };
            res.send(mustache.render(userTemplate, Data, partials))
            //res.send(req[catProperty]);
        }
        else {
            res.sendStatus(404);
        }

    },
    getCatHandler(req, res, next) {
        const cat = repository.getCatById(parseInt(req.params.id));
        if (cat) {
            req[catProperty] = cat;
            console.log(req[catProperty]);
            next();
        }
        else {
            res.sendStatus(404);
        }
    },
    deleteCatbyId(req, res) {
        repository.deleteCat(parseInt(req.params.id));
        res.send(req[catProperty])
    },
    addCat(req, res) {
        //console.log(req.files);
        let cat={};
        cat.name=req.body.name;
        cat.breed=req.body.breed;
        cat.atAge = req.body.age;
        cat.weight = req.body.weight;
        cat.registeredAt=req.body.register;
        console.log(req.files);
        let name=req.files['avaUrl'].name.split('.');
        let media={};
        media.filename='cat'+repository.storage.nextId+'.'+name[1];
        console.log(media.filename);
        fs.writeFileSync('./data/media/'+media.filename, req.files['avaUrl'].data);
        cat.avaUrl='/media/'+media.filename;
        if (!('name' in cat) || !('breed' in cat) || !('atAge' in cat) ||
            !('weight' in cat) || !('registeredAt' in cat)) {
            res.sendStatus(400);
        }
        else {
            id = repository.addCat(cat);
            cat.id = id;
            req[catProperty] = cat;
            res.redirect('/api/cats/'+id);
        }
    },
    updateCat(req, res) {
        const cat = repository.getCatById(parseInt(req.params.id));
        console.log(req.files);
        fs.writeFileSync('./returned/cat.json', req.files['cat'].data);
        const jsonText = fs.readFileSync('./returned/cat.json');
        const jsonArray = JSON.parse(jsonText);
        if ('name' in jsonArray) cat.name = jsonArray.name;
        if ('breed' in jsonArray) cat.breed = jsonArray.breed;
        if ('atAge' in jsonArray) cat.atAge = jsonArray.atAge;
        if ('weight' in jsonArray) cat.weight = jsonArray.weight;
        if ('registeredAt' in jsonArray) cat.registeredAt = jsonArray.registeredAt;
        repository.updateCat(cat);
        req[catProperty] = cat;
        res.send(req[catProperty])
    },
    new(req, res) {
        const head = fs.readFileSync("./views/partials/head.mst").toString();
        const header = fs.readFileSync("./views/partials/header.mst").toString();
        const footer = fs.readFileSync("./views/partials/footer.mst").toString();
        const userTemplate = fs.readFileSync("./views/new.mst").toString();

        const Data = {
            links: [{
                l: "/",
                name: "Home"
            },
            {
                l: "/api/cats",
                name: "Cats"
            },
            {
                l: "/api/users",
                name: "Users"
            },
            {
                l: "/api/about",
                name: "About"
            }
            ]
        };
        const partials = {
            head: head,
            header: header,
            footer: footer
        };
        res.send(mustache.render(userTemplate, Data, partials))
    }
};