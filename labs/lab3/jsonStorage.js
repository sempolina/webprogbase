const fs = require('fs');
 
class JsonStorage {

    constructor(filePath) {
        this.filePath = filePath;
    }
 
    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.items;
    }
    get nextId() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.nextId;
    }

    incrementNextId() {
       return (this.nextId +1);
    }
    writeItems(items) {
        let next=this.nextId;
        if(items.length>this.readItems().length){
            next=this.incrementNextId()
        }
        const object ={
            nextId: next,
            items:items,
        };
        let jsonText= JSON.stringify(object,null,4);
        fs.writeFileSync(this.filePath,jsonText);
    }
};
 
module.exports = JsonStorage;
