function vigenereCipher(text, key) {
    text = text.toUpperCase()
    key = key.toUpperCase()
    text = text.replace(/[^A-Z]/g, "")
    key = key.replace(/[^A-Z]/g, "")
    let cip =
    {
        key: key,
        message: text,
        encoded_message: "",
    };
    for (let i = 0; i < text.length; i++) {
        cip.encoded_message += String.fromCharCode((cip.message[i].charCodeAt(0) - 2 * "A".charCodeAt(0) + cip.key[i % key.length].charCodeAt(0)) % 26 + "A".charCodeAt(0))
    }
    return cip;
}
function vigenereDecipher(cipher, key) {
    cipher = cipher.toUpperCase()
    cipher = cipher.replace(/[^A-Z]/g, "")
    let cip =
    {
        key: key,
        message: "",
        encoded_message: cipher,
    };
    if (key == undefined) {
        let freq = [8.167, 1.492, 2.782, 4.253, 12.70, 2.228, 2.015, 6.094, 6.966, 0.153, 0.772, 4.025, 2.406, 6.749, 7.507, 1.929, 0.095, 5.987, 6.327, 9.056, 2.758, 0.978, 2.361, 0.150, 1.974, 0.074]
        let k =
        {
            i: 1,
            diff: 0,
        };
        for (let i = 2; i < 128; i++) {
            k2 = 0;
            let len = 0;
            let numbers = []
            for (let j = 0; j < cip.encoded_message.length; j += i) {
                if (numbers[cip.encoded_message[j].charCodeAt(0) - "A".charCodeAt(0)] == undefined) {
                    numbers[cip.encoded_message[j].charCodeAt(0) - "A".charCodeAt(0)] = 0;
                }
                numbers[cip.encoded_message[j].charCodeAt(0) - "A".charCodeAt(0)]++;
                len++;
            }
            for (let j = 0; j < 26; j++) {
                if (numbers[j] == undefined) {
                    numbers[j] = 0
                }
                k2 += numbers[j] * (numbers[j] - 1) / (len * (len - 1))
            }
            if (Math.abs(k2 - 0.065) < Math.abs(k.i - 0.065)) {
                k.i = k2;
                k.diff = i
            }
        }

        let code = ""
        for (let i = 0; i < k.diff; i++) {
            let alpha = []
            let str = ""
            for (let j = i; j < cip.encoded_message.length; j += k.diff) {
                str += cip.encoded_message[j]
                if (alpha[cip.encoded_message[j].charCodeAt(0) - "A".charCodeAt(0)] == undefined) {
                    alpha[cip.encoded_message[j].charCodeAt(0) - "A".charCodeAt(0)] = 0;
                }
                alpha[cip.encoded_message[j].charCodeAt(0) - "A".charCodeAt(0)]++;
            }
            let shift =
            {
                sum: 10000000,
                i: 0,
            }
            for (let j = 0; j < 26; j++) {
                let sum = 0
                for (let m = 0; m < 26; m++) {
                    if (alpha[(m + j) % 26] == undefined) {
                        alpha[(m + j) % 26] = 0
                    }
                    sum += Math.abs(freq[(m) % 26] / 100 - alpha[(m + j) % 26] / str.length)
                }
                if (sum < shift.sum) {
                    shift.sum = sum
                    shift.i = j
                }
            }
            code += String.fromCharCode("A".charCodeAt(0) + shift.i)
        }
        cip.key=code
    }
    cip.key = cip.key.toUpperCase()
    cip.key = cip.key.replace(/[^A-Z]/g, "")
    for (let i = 0; i < cipher.length; i++) {
        let code = cip.encoded_message[i].charCodeAt(0) - cip.key[i % cip.key.length].charCodeAt(0)
        if (code < 0) code += 26
        cip.message += String.fromCharCode(code + "A".charCodeAt(0))
    }
    return cip
}