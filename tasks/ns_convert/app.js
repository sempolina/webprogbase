
function nsConvert(num, baseFrom=10, baseTo=2) {
    if(baseFrom!==10) {
      let len=num.length;
      let num2=0
      for(let i=0;i<len;i++)
      {
        if(!(+(num[len-1-i])))
        {
          num2+=(num[len-1-i].charCodeAt(0)-"A".charCodeAt(0)+10)*(baseFrom**i)
        }
        else{
          num2+=(+(num[len-1-i]))*(baseFrom**i)
        }
      }
      num=num2
    }
    if(baseTo!==10)
    {
      let num2=""
      let i=1
      while(num>0)
      {
        if(num%baseTo>9)
        {
          num2=String.fromCharCode("A".charCodeAt(0)+num%baseTo-10)+num2
        }
        else{
          num2=num%baseTo+num2
        }
        num=Math.trunc(num/baseTo)
      }
      num=num2
    }
    return num
}  
console.assert(nsConvert("23", 10, 2) === "10111", "Check 1");
console.assert(nsConvert("31", 10, 16) === "1F", "Check 2");
console.assert(nsConvert("1F", 16, 2) === "11111", "Check 3");
console.log("Finished all checks.");